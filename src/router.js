import { createWebHistory, createRouter } from "vue-router";
import Dashboard from "./views/Dashboard.vue";
import Settings from "./views/Settings.vue";
import Stats from "./views/Stats.vue";
import Manage from "./views/Manage.vue";
import User from "./views/User.vue";

const routes = [
  {
    path: "/",
    name: "Dashboard",
    component: Dashboard,
  },
  {
    path: "/Settings",
    name: "Settings",
    component: Settings,
  },
  {
    path: "/Stats",
    name: "Statistics",
    component: Stats,
  },
  {
    path: "/Manage",
    name: "Manage",
    component: Manage,
  },
  {
    path: "/User",
    name: "User",
    component: User,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
